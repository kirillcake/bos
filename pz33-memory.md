## Практическое занятие №N. Иерархия памяти и разная скорость доступа к ней (программа для замера скорости). Именованные и неименованные каналы, разница и применения.

### Термины, которые нужно усвоить
1. Иерархия памяти
2. Регистр процессора
1. Кэш память
1. Именованные каналы
1. Неименованные каналы

### Теория
#### Иерархия памяти 

Большинство программистов представляют вычислительную систему как процессор, который выполняет инструкции, и память, которая хранит инструкции и данные для процессора. В этой простой модели память представляется линейным массивом байтов и процессор может обратиться к любому месту в памяти за константное время. Хотя это эффективная модель для большинства ситуаций, она не отражает того, как в действительности работают современные системы.

В действительности система памяти образует иерархию устройств хранения с разными ёмкостями, стоимостью и временем доступа. 

![memory](img/memory.png)


Регистры процессора хранят наиболее часто используемые данные. Маленькие быстрые кэш-памяти, расположенные близко к процессору, 
служат буферными зонами, которые хранят маленькую часть данных, расположенных в относительно медленной оперативной памяти. 
Оперативная память служит буфером для медленных локальных дисков. А локальные диски служат буфером для данных с удалённых машин, связанных сетью.

В большинстве современных ПК используется следующая иерархия памяти:

Регистры процессора, организованные в регистровый файл — наиболее быстрый доступ (порядка 1 такта), но размером лишь в несколько сотен или, редко, тысяч байт.

Кэш процессора 1го уровня (L1) — время доступа порядка нескольких тактов, размером в десятки килобайт.

Кэш процессора 2го уровня (L2) — большее время доступа (от 2 до 10 раз медленнее L1), около полумегабайта или более.

Кэш процессора 3го уровня (L3) — время доступа около сотни тактов, размером в несколько мегабайт (в массовых процессорах используется недавно).

ОЗУ системы — время доступа от сотен до, возможно, тысячи тактов, но огромные размеры в несколько гигабайт, вплоть до сотен. Время доступа к ОЗУ может варьироваться для разных его частей в случае комплексов класса NUMA (с неоднородным доступом в память).
Дисковое хранилище — многие миллионы тактов, если данные не были закэшированны или забуферизованны заранее, размеры до нескольких терабайт.
Третичная память — задержки до нескольких секунд или минут, но практически неограниченные объёмы (ленточные библиотеки).

Регистр процессора — блок ячеек памяти, образующий сверхбыструю оперативную память (СОЗУ) внутри процессора; используется самим процессором и большей частью недоступен программисту: например, при выборке из памяти очередной команды она помещается в регистр команд, к которому программист обратиться не может.
Имеются также регистры, которые, в принципе, программно доступны, но обращение к ним осуществляется из программ операционной системы, например, управляющие регистры и теневые регистры дескрипторов сегментов. Этими регистрами пользуются в основном разработчики операционных систем.
Существуют также так называемые регистры общего назначения (РОН), представляющие собой часть регистров процессора, использующихся без ограничения в арифметических операциях, но имеющие определённые ограничения

Кэш микропроцессора — кэш (сверхоперативная память), используемый микропроцессором компьютера для уменьшения среднего времени доступа к компьютерной памяти. Является одним из верхних уровней иерархии памяти. Кэш использует небольшую, очень быструю память (обычно типа SRAM), которая хранит копии часто используемых данных из основной памяти. Если большая часть запросов в память будет обрабатываться кэшем, средняя задержка обращения к памяти будет приближаться к задержкам работы кэша.
Когда процессору нужно обратиться в память для чтения или записи данных, он сначала проверяет, доступна ли их копия в кэше. В случае успеха проверки процессор производит операцию, используя кэш, что значительно быстрее использования более медленной основной памяти. Подробнее о задержках памяти см. Латентность SDRAM: tCAS, tRCD, tRP, tRAS.
Данные между кэшем и памятью передаются блоками фиксированного размера, также называемые линиями кэша (англ. cache line) или блоками кэша.
Большинство современных микропроцессоров для компьютеров и серверов имеют как минимум три независимых кэша: кэш инструкций для ускорения загрузки машинного кода, кэш данных для ускорения чтения и записи данных и буфер ассоциативной трансляции (TLB) для ускорения трансляции виртуальных (логических) адресов в физические, как для инструкций, так и для данных. Кэш данных часто реализуется в виде многоуровневого кэша (L1, L2, L3).

#### Каналы (pipe,fifo)
Каналы - неименованные (pipe) и именованные (fifo) - это средство передачи данных между процессами.

Можно представить себе канал как небольшой кольцевой буфер в ядре операционной системы. С точки зрения процессов, канал выглядит как пара открытых файловых дескрипторов – один на чтение и один на запись (можно больше, но неудобно). Мы можем писать в канал до тех пор, пока есть место в буфере, если место в буфере кончится – процесс будет заблокирован на записи. Можем читать из канала, пока есть данные в буфере, если данных нет – процесс будет заблокирован на чтении. Если закрыть дескриптор, отвечающий за запись, то попытка чтения покажет конец файла. Если закрыть дескриптор, отвечающий за чтение, то попытка записи приведет к доставке сигнала SIGPIPE и ошибке EPIPE.

Понятия позиции чтения/записи для каналов не существует, поэтому запись всегда производится в хвост буфера, а чтение - с головы.

Самым простым примером использования канала является оператор '|' в Linux.

```bash
	netstat -na | grep :22
```
В примере выше производится поиск открытого 80го порта на устройстве.

Неименованные каналы
Неименованный канал создается вызовом pipe, который заносит в массив int [2] два дескриптора открытых файлов. fd[0] – открыт на чтение, fd[1] – на запись (вспомните STDIN == 0, STDOUT == 1). Канал уничтожается, когда будут закрыты все файловые дескрипторы, ссылающиеся на него.

В рамках одного процесса pipe смысла не имеет, передать информацию о нем в произвольный процесс нельзя (имени нет, а номера файловых дескрипторов в каждом процессе свои). Единственный способ использовать pipe – унаследовать дескрипторы при вызове fork (и последующем exec). После вызова fork канал окажется открытым на чтение и запись в родительском и дочернем процессе. Т.е. теперь на него будут ссылаться 4 дескриптора. Теперь надо определиться с направлением передачи данных – если надо передавать данные от родителя к потомку, то родитель закрывает дескриптор на чтение, а потомок - дескриптор на запись.

##### Пример программы, использующей системные вызовы Linux для работы с каналами

```cpp
int fd[2];
char c;
pipe(fd);
if( fork() ) { //родитель
    close(fd[0]);
    c=0;
    while(write(fd[1],&c,1) >0)  {
         c++;
     }
} else { //дочерний процесс
    dup2(fd[0],0); //подменили STDIN
    close(fd[0]);
    close(fd[1]);
    execl("prog","prog",NULL); //запустили новую программу для которой STDIN = pipe
}
```
Оставлять оба дескриптора незакрытыми плохо по двум причинам:

Родитель после записи не может узнать, считал ли дочерний процесс данные, а если считал, то сколько. Соответственно, если родитель попробует читать из pipe, то, вполне вероятно, он считает часть собственных данных, которые станут недоступными для потомка.

Если один из процессов завершился или закрыл свои дескрипторы, то второй этого не заметит, так как pipe на его стороне по-прежнему открыт на чтение и на запись.

Если надо организовать двунаправленную передачу данных, то можно создать два pipe.

Именованные каналы
Именованный канал FIFO доступен как объект в файловой системе. При этом, до открытия объекта FIFO на чтение, собственно коммуникационного объекта не создаётся. После открытия объекта FIFO в одном процессе на чтение, а в другом на запись, возникает ситуация полностью эквивалентная использованию неименованного канала. FIFO создаётся вызовом функции int mkfifo(const char *pathname, mode_t mode);,

Основное отличие между pipe и FIFO - то, что pipe могут совместно использовать только процессы, находящиеся в отношении родительский-дочерний, а FIFO может использовать любая пара процессов.

Правила обмена через канал
Чтение:

При чтении числа байт, меньшего чем находится в канале, возвращается требуемое число байтов, остаток сохраняется для последующих чтений.
При чтении числа байт, большего чем находится в канале, возвращается доступное число байт.
При чтении из пустого канала, открытого каким-либо процессом на запись, при сброшенном флаге O_NONBLOCK произойдёт блокировка процесса, а при установленном флаге O_NONBLOCK будет возвращено -1 и установлено значение errno, равное EAGAIN.
Если канал пуст, и ни один процесс не открыл его на запись, то при чтении из канала будет получено 0 байтов - т.е конец файла.
Запись:

Если процесс пытается записать данные в канал, не открытый ни одним процессом на чтение, то процессу отправляется сигнал SIGPIPE. Если не установлена обработка сигнала, то процесс завершается, в противном случае вызов write() возвращает -1 с установкой ошибки EPIPE.
Запись числа байт меньше чем PIPE_BUF выполняется атомарно. При записи из нескольких процессов данные не перемешиваются.
При записи числа байт больше чем PIPE_BUF атомарность операции не гарантируется.
Если флаг O_NONBLOCK не установлен, то запись может быть заблокирована, но в конце концов будет возвращено значение, указывающее, что все байты записаны.
Если флаг O_NONBLOCK установлен и записывается меньше чем PIPE_BUF, то возможны два варианта: если есть достаточно свободного места в буфере, то производится атомарная запись, если нет, то возвращается -1, а errno выставляется в EAGAIN.
Если флаг O_NONBLOCK установлен и записывается больше чем PIPE_BUF, то возможны два варианта: если в буфере есть хотя бы один свободный байт, то производится запись доступного числа байт, если нет, то возвращается -1, а errno выставляется в EAGAIN.

### Ход работы

#### Каналы (pipe,fifo) (практика)

1. Для выполнения практической части работы необходима виртуальная машина с linux (например, ubuntu 18.04)
2. Перед выполнением необходимо создать пользователя *фамилия*_*номергруппы*, выполнив следующую команду или любым другим способом (при необходимости использовать sudo)
```bash
	adduser surname_73**
	#пароль состава и длины на выбор слушателя
```

3. Дальнейшие команды выполнять из-под созданного пользователя

4. В терминале выполнить команду, демонстрирующую работы системных каналов.
```bash
	netstat -na | grep :22
```
5. Напишите программу на c/с++, использующую механизм каналов. В качестве основы используйте код из теоретического материала. 
6. Создайте директорию в домашнем каталоге, имя директории - номер по списку. В каталоге создайте файл для исходного кода, например, "test.cpp".
```bash
	mkdir 7
	cd 7
	touch test.cpp
``` 
7. Используя любой текстовый редактор, скопируйте следующий код:
```cpp
#include <iostream>
#include <sys/wait.h>
#include <unistd.h>
int main() {
    int pipefd[2];
    if (pipe(pipefd) < 0) {
        perror("error on pipe"); exit(1);
    }
    pid_t pid_left = fork();
    if (pid_left == 0) {
        close(pipefd[0]);
        close(1);
        dup(pipefd[1]);
        close(pipefd[1]);
        execl("/bin/cat", "cat", "test.cpp", NULL);
        exit(0);
    }
    pid_t pid_right = fork();
    if (pid_right == 0) {
        close(pipefd[1]);
        close(0);
        dup(pipefd[0]);
        close(pipefd[0]);
        const int bufsize = 1024;
        char str[bufsize];
        execl("/bin/grep", "grep", "pipe", NULL);

        exit(0);
    }
    close(pipefd[0]);
    close(pipefd[1]);
    int status;
    wait(&status);
    wait(&status);

    return 0;
}
```
8. Код программы объединяет 3 процесса в конвейер (родитель и два потомка, левый и правый), в котором поток стандартного вывода левого потомка соединен через канал с потоком стандартного ввода правого потомка.
9. В результате выполнения программы в консоль будут выведены все строки файла исходного кода, содержащие строку "pipe".

10. Для компиляции и запуска приложения используйте следующие команды:
```bash
	g++ -o test test.cpp
	chmod +x test
	./test
``` 

##### Задание для самостоятельной работы

1. Внесите изменения в команду таким образом, чтобы в консоль вывелась информация о всех портах, открытых на прослушивание (В столбце State - LISTEN).

2. Внесите изменения в команду таким образом, чтобы в консоль вывелась информация о всех процессах, которые запущены от имени текущего пользователя.

3. Доработайте программу "test" таким образом, чтобы результат выполнения был идентичен команде 
```bash
	netstat -na | grep :22
```

#### Иерархия памяти (практика)

1. Определите количество уровней кэша процессора используемого компьютера и объём каждого из уровней. 
   (Для ОС Windows информацию можно найти в диспетчере задач на вкладке производительности).

2. Напишите программу, считывающую массив целочисленных переменных из файла и из оперативной памяти, сравните время обращения.
   Процесс создания файла исходного кода и компиляции аналогичен программе "test".
   Входной файл создаётся отдельно и заполняется необходимым количеством целочисленных переменных. (Конечно отдельной программой, код которой додумайте самостоятельно).

```cpp
#include <stdio.h>
#include <stdlib.h>
#include <cstdlib>
#include <time.h>
int main()
{
	unsigned int start_time, end_time, search_time;
	FILE *fileBuf;

	srand(time(NULL));
    size = 100000; // номер по журналу * 100000
	int buf[size];
	int mas[size];

	for (int i = 0; i < size; i++)
	{
	  buf[i] = rand()%7334;/*номер группы*/
	}

	fileBuf = fopen("fileBuf.surname", "r"); /*в качестве расширения использовать фамилию слушателя*/

	start_time =  clock(); 
	for (int i = 0; i < size; i++)
	{
	  fscanf(fileBuf, "%d", &mas[i]);
	}
	end_time = clock(); 
	
	search_time = end_time - start_time; 
	
	printf("Time from file: %d ms\n", search_time);
	
	start_time =  clock(); 
	for (int i = 0; i < size; i++)
	{
	  mas[i] = buf[i];
	}
	end_time = clock(); 
	
	search_time = end_time - start_time; 
	
	printf("Time from  mas: %d ms\n", search_time);
	
	fclose(fileBuf);
	return 0;
}
```

##### Задание для самостоятельной работы

1. Внесите изменения в программу таким образом, чтобы выводилось время обращения к одному элементу.

2. Внесите изменения в программу таким образом, чтобы время обращения к элементу выводилось в нс.